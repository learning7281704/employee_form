// import * as  abc from "./temp";
var Gender;
(function (Gender) {
    Gender["male"] = "male";
    Gender["female"] = "female";
})(Gender || (Gender = {}));
var emp_data = [];
window.onload = function () {
    //   console.log(abc);
    $("#submit").on("click", getData);
    $("#card-holder").html("");
    for (var i = 0; i < emp_data.length; ++i) {
        append_card(emp_data[i]);
    }
};
var validate_data = function (data) {
    if (typeof data["name"] != "string" || data["name"].length < 5)
        return false;
    if (typeof data["email"] != "string" || data["email"].length < 5)
        return false;
    if (typeof data["fun_fact"] != "string" || data["fun_fact"].length < 5)
        return false;
    if (typeof data["linkedin"] != "string" || data["linkedin"].length < 5)
        return false;
    if (typeof data["gender"] != "string" ||
        ["male", "female", "other"].indexOf(data["gender"]) == -1)
        return false;
    if (typeof data["age"] != "number" || data["age"] < 18)
        return false;
    return true;
};
var getData = function () {
    var name = $("#inp-name").val();
    var email = $("#inp-email").val();
    var fun_fact = $("#inp-desc").val();
    var linkedin = $("#inp-linkedin").val();
    var gender = $("#inp-gender").val();
    var age = $("#inp-age").val();
    age = parseInt(age, 10);
    var data = {
        name: name,
        email: email,
        fun_fact: fun_fact,
        linkedin: linkedin,
        gender: gender,
        age: age,
    };
    console.log(data);
    if (validate_data(data) === false) {
        alert("Please enter appropriate data");
    }
    else {
        emp_data.push(data);
        console.log(emp_data);
        append_card(data);
    }
};
var append_card = function (data) {
    var text = "\n        <div class=\"card\" style=\"width: 18rem\">\n            <div class=\"card-body\">\n            <h5 class=\"card-title\">".concat(data["name"], "</h5>\n            <h6 class=\"card-subtitle mb-2 text-muted\">\n                <span class=\"age\">").concat(data["age"], "</span>\n                :\n                <span class=\"gender\">").concat(data["gender"], "</span>\n            </h6>\n            <p class=\"card-text\">\n            ").concat(data["fun_fact"], "\n            </p>\n            <a href=\"mailto:").concat(data["email"], "\" class=\"card-link\">Email</a>\n            <a href=\"").concat(data["linkedin"], "\" target=\"_blank\" class=\"card-link\">Linkedin</a>\n            </div>\n      </div>");
    $("#card-holder").append(text);
};
