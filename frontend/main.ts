// import * as  abc from "./temp";
enum Gender {
  male = "male",
  female = "female",
}

type Employee = {
  name: string;
  email: string;
  fun_fact: string;
  linkedin: string;
  age: number;
  gender: Gender;
};

let emp_data: Employee[] = [];

window.onload = () => {
  //   console.log(abc);

  $("#submit").on("click", getData);
  $("#card-holder").html("");
  for (let i = 0; i < emp_data.length; ++i) {
    append_card(emp_data[i]);
  }
};

const validate_data = (data: Employee): boolean => {
  if (typeof data["name"] != "string" || data["name"].length < 5) return false;
  if (typeof data["email"] != "string" || data["email"].length < 5)
    return false;
  if (typeof data["fun_fact"] != "string" || data["fun_fact"].length < 5)
    return false;
  if (typeof data["linkedin"] != "string" || data["linkedin"].length < 5)
    return false;
  if (
    typeof data["gender"] != "string" ||
    ["male", "female", "other"].indexOf(data["gender"]) == -1
  )
    return false;
  if (typeof data["age"] != "number" || data["age"] < 18) return false;
  return true;
};

const getData = (): void => {
  let name: string = $("#inp-name").val() as string;
  let email: string = $("#inp-email").val() as string;
  let fun_fact: string = $("#inp-desc").val() as string;
  let linkedin: string = $("#inp-linkedin").val() as string;
  let gender: Gender = $("#inp-gender").val() as Gender;
  let age: any = $("#inp-age").val() as number;

  age = parseInt(age, 10);

  let data: Employee = {
    name,
    email,
    fun_fact,
    linkedin,
    gender,
    age,
  };
  console.log(data);

  if (validate_data(data) === false) {
    alert("Please enter appropriate data");
  } else {
    emp_data.push(data);
    console.log(emp_data);

    append_card(data);
  }
};

const append_card = (data: Employee): void => {
  let text: string = `
        <div class="card" style="width: 18rem">
            <div class="card-body">
            <h5 class="card-title">${data["name"]}</h5>
            <h6 class="card-subtitle mb-2 text-muted">
                <span class="age">${data["age"]}</span>
                :
                <span class="gender">${data["gender"]}</span>
            </h6>
            <p class="card-text">
            ${data["fun_fact"]}
            </p>
            <a href="mailto:${data["email"]}" class="card-link">Email</a>
            <a href="${data["linkedin"]}" target="_blank" class="card-link">Linkedin</a>
            </div>
      </div>`;

  $("#card-holder").append(text);
};
